// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCfUdRVttOgMgWpZsnbkRwxSu9Y-1NgbMU",
    authDomain: "nilm-project-48e8c.firebaseapp.com",
    projectId: "nilm-project-48e8c",
    storageBucket: "nilm-project-48e8c.appspot.com",
    messagingSenderId: "376382069872",
    appId: "1:376382069872:web:4e6889f79d73d9942ae8b5"
  },
  flaskhost: 'http://127.0.0.1:5000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
