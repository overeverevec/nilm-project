import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AngularFireDatabase , AngularFireList } from "@angular/fire/database"
@Injectable({
  providedIn: 'root'
})
export class DeviceService {


  constructor(private firebase: AngularFireDatabase) { }

  deviceList:AngularFireList<any>;

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    deviceName: new FormControl('', Validators.required),
  });


  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      deviceName: '',
    });
  }

  getDevice(){
    this.deviceList = this.firebase.list('device')
    return this.deviceList.snapshotChanges();
  }

  insertDevice(device) {
    this.deviceList.push({
      deviceName: device.deviceName,
    });
  }

  updateDevice(device){
    this.deviceList.update(device.$key,{
      deviceName: device.deviceName
    })
  }

  deleteDevice($key:string){
    this.deviceList.remove($key);
  }

  populateForm(device) {
    this.form.setValue(device);
  }



}
