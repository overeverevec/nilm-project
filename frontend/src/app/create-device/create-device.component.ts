import { Component, OnInit , Inject, ɵConsole } from '@angular/core';
// import { MatDialog,MatDialogRef} from '@angular/material/dialog';
import { FormGroup,FormControl} from'@angular/forms';
import {MatDialogRef} from '@angular/material/dialog'
import { DeviceService } from '../device.service'
import { HttpClient } from '@angular/common/http';
import { environment}  from '../../environments/environment'

@Component({
  selector: 'app-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.css']
})



export class CreateDeviceComponent  {
  constructor(public service:DeviceService,
              public dialogRef :MatDialogRef<CreateDeviceComponent>,
              private http:HttpClient){}
  
  public url = environment.flaskhost;
  ngOnInit() {
     this.service.getDevice()
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    // console.log('clear')

  }

  onSubmit(){
    if(this.service.form.valid){
      // console.log(this.service.form.get('$key').value)
      if (!this.service.form.get('$key').value)
        this.service.insertDevice(this.service.form.value);
      
      else
        this.service.updateDevice(this.service.form.value);
      
      this.service.form.reset();
      this.service.initializeFormGroup();
      this.onClose();
      this.updateBackend()

    }
  }
  
  updateBackend(){
    let results
    this.http.get(this.url+'/updatedata').subscribe(data => {
      results = data;
    });
    // console.log(results)    
  }

  onClose(){
    this.service.form.reset();
    this.service.initializeFormGroup();
    this.dialogRef.close();
  }
  
  
  delay(delayInms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }


}
