
export class dataCollect {
    active: {
        hour: Number
    }
    current: {
        hour: Number
    }
    voltage: {
        hour: Number
    }
    frequency: {
        hour: Number
    }
    reactive: {
        hour: Number
    }
    apparent: {
        hour: Number
    }
}
export class dataSelect {
    datetime:{
        date:Date

    }	
    active:{
        active:Number

    }	
    reactive:{
        reactive:Number

    }	
    apparent:{
        apparent:Number

    }	
    hour:{
        hour:Number

    }
}
export class dataPercent {
    "percen_on": {
        "1s": {
            percent:Number
        }
        
        "10s": {
            percent:Number
        }
        "30s": {
            percent:Number
        }
        "1M": {
            percent:Number
        }
        "5M": {
            percent:Number
        }
        "15M": {
            percent:Number
        }
        "30M": {
            percent:Number
        }
        "1H": {
            percent:Number
        }
        "2H": {
            percent:Number
        }
        "4H": {
            percent:Number
        }
        ">4H": Number
    }
    
}

interface num{
    "0": Date
    "1": Date
    "2": Date
    "3": Date
    "4": Date
    "5": Date
    "6": Date
    "7": Date
    "8": Date
    "9": Date
    "10": Date
    "11": Date
    "12": Date
    "13": Date
    "14": Date
    "15": Date
    "16": Date
    "17": Date
    "18": Date
    "19": Date
    "20": Date
    "21": Date
    "22": Date
    "23": Date
}

interface num_realtime{
    "0": number
    "1": number
    "2": number
    "3": number
    "4": number
    "5": number
    "6": number
    "7": number
    "8": number
    "9": number
    "10": number
    "11": number
    "12": number
    "13": number
    "14": number
    "15": number
    "16": number
    "17": number
    "18": number
    "19": number
    "20": number
    "21": number
    "22": number
    "23": number
    "24": number
    "25": number
    "26": number
    "27": number
    "28": number
    "29": number
    "30": number
    "31": number
    "32": number
    "33": number
    "34": number
    "35": number
    "36": number
    "37": number
    "38": number
    "39": number
    "40": number
    "41": number
    "42": number
    "43": number
    "44": number
    "45": number
    "46": number
    "47": number
    "48": number
    "49": number
    "50": number
    "51": number
    "52": number
    "53": number
    "54": number
    "55": number
    "56": number
    "57": number
    "58": number
    "59": number
    "60": number
    "61": number
    "62": number
    "63": number
    "64": number
    "65": number
    "66": number
    "67": number
    "69": number
    "70": number
    "71": number
    "72": number
    "73": number
    "74": number
    "75": number
    "76": number
    "77": number
    "78": number
    "79": number
    "80": number
    "81": number
    "82": number
    "83": number
    "84": number
    "85": number
    "86": number
    "87": number
    "88": number
    "89": number
    "90": number
    "91": number
    "92": number
    "93": number
    "94": number
    "95": number
    "96": number
}
 

interface date_realtime{
    "0": Date
    "1": Date
    "2": Date
    "3": Date
    "4": Date
    "5": Date
    "6": Date
    "7": Date
    "8": Date
    "9": Date
    "10": Date
    "11": Date
    "12": Date
    "13": Date
    "14": Date
    "15": Date
    "16": Date
    "17": Date
    "18": Date
    "19": Date
    "20": Date
    "21": Date
    "22": Date
    "23": Date
    "24": Date
    "25": Date
    "26": Date
    "27": Date
    "28": Date
    "29": Date
    "30": Date
    "31": Date
    "32": Date
    "33": Date
    "34": Date
    "35": Date
    "36": Date
    "37": Date
    "38": Date
    "39": Date
    "40": Date
    "41": Date
    "42": Date
    "43": Date
    "44": Date
    "45": Date
    "46": Date
    "47": Date
    "48": Date
    // "49": Date
    // "50": Date
    // "51": Date
    // "52": Date
    // "53": Date
    // "54": Date
    // "55": Date
    // "56": Date
    // "57": Date
    // "58": Date
    // "59": Date
    // "60": Date
    // "61": Date
    // "62": Date
    // "63": Date
    // "64": Date
    // "65": Date
    // "66": Date
    // "67": Date
    // "69": Date
    // "70": Date
    // "71": Date
    // "72": Date
    // "73": Date
    // "74": Date
    // "75": Date
    // "76": Date
    // "77": Date
    // "78": Date
    // "79": Date
    // "80": Date
    // "81": Date
    // "82": Date
    // "83": Date
    // "84": Date
    // "85": Date
    // "86": Date
    // "87": Date
    // "88": Date
    // "89": Date
    // "90": Date
    // "91": Date
    // "92": Date
    // "93": Date
    // "94": Date
    // "95": Date
    // "96": Date
}

export class dataFreq {
    "count": num
    "hour": num
    "percent": num
}

export class realtime {
    "device1": num_realtime
    "device2":num_realtime
    "device3": num_realtime
    "Datetime":num_realtime
}
export class realtime_appliance  {
    "timestamp": string
    "device1": number
    "device2": number
    "device3": number
}

export class h_appliance {
    "device1": num
    "device2": num
    "device3": num

}
interface week{
    "Monday": Number,
    "Tuesday": Number,
    "Wednesday": Number,
    "Thursday": Number,
    "Friday": Number,
    "Saturday": Number,
    "Sunday": Number,
}
 export class history_w{
        "fridge": week
        "iron": week
        "wash": week
        "tv": week
        "air": week
        "wet": week
        "com": week
 }

interface appliance{
    "fridge": Date,
    "iron": Date,
    "air": Date,
    "com": Date,
    "tv": Date,
    "wash": Date,
    "wet": Date,
}

export class datamissing{
    "0": appliance
    "1": appliance
    "2": appliance
    "3": appliance
    "4": appliance
    "5": appliance
    "6": appliance
    "7": appliance
    "8": appliance
    "9": appliance
    "10": appliance
    "11": appliance
    "12": appliance
    "13": appliance
    "14": appliance
    "15": appliance
    "16": appliance
    "17": appliance
    "18": appliance
    "19": appliance
    "20": appliance
    "21": appliance
    "22": appliance
    "23": appliance
}