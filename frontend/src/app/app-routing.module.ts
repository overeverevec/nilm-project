import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { ClientComponent } from './client/client.component';

import { SettingComponent } from './setting/setting.component';
import { ApplianceComponent } from './appliance/appliance.component';
import { CreateDeviceComponent } from './create-device/create-device.component';

const routes: Routes = [
  { path: '', component: ClientComponent },
  { path: 'meter', component: ApplianceComponent },
  { path: 'setting', component: SettingComponent },
  { path: 'setting/create', component: CreateDeviceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
