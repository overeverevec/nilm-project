import { Component, OnInit } from '@angular/core';
import { ClientService} from '../client/client.service';
import { h_appliance, realtime} from '../interfaces/data.interface';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { DeviceService } from '../device.service'
import { environment}  from '../../environments/environment'

@Component({
  selector: 'app-appliance',
  templateUrl: './appliance.component.html',
  styleUrls: ['./appliance.component.css'],
  providers: [ClientService]
})


export class ApplianceComponent  {
  checkoutForm;
  constructor(
    private http: HttpClient,
    private clientService: ClientService,  
    private service:DeviceService,
  
  ) {}
  public hours = ['12PM','','','3AM','','','6AM',
  '','','9AM','','','12PM','','','3PM',
  '','','6PM','','','9PM','','']
  
  public marked = true;
  public Checkbox_device1 = true;
  public Checkbox_device2 = true;
  public Checkbox_device3 = true;
  
  public all_active:h_appliance[];
   //select date
  public dateOfStart:Date ; 
  public dateOfEnd:Date; 
  public url = environment.flaskhost;
  public postData:any;
  public minDate  = new Date(2021,2,1)
  public maxDate  = new Date();
  public dataSelect= [];
  public dataReactive = [];
  public datafreq = [];
  public dataduration = [];

  public dv = [];
  public device_reactive = [];
  public device_freq = [];
  public device_duration = [];
  public checkbox = [];
  public device_name =[];
  public colorlist = ['#542e71', '#fb3640', '#fdca40', '#a799b7',
  '#ffc996', '#ff8474','#9f5f80', '#583d72',
  '#194350', '#ff8882', '#ffc2b4', '#9dbeb9',
  '#4b778d', '#28b5b5', '#8fd9a8', '#d2e69c'
  ]
   public selectedIndex = -1;
   checked: boolean
  // for name
  public array;
  public label = [];
  public cb = [];


async ngOnInit() {

  //get name
    let device_name = await this.clientService.getDevicename() 
    

    await this.delay(100); 
    for (var i in device_name[0]){
      this.cb.push(true)
      this.checkbox.push({name:device_name[0][0][i],checked:true})
    }
    await this.delay(100);
    this.device_name = device_name[0]
  //select date
  this.dateOfStart =new Date("2021-03-01");
  this.dateOfEnd = new Date();
  this.sendDate()

}
  toggleVisibility(e,i){
    // console.log(e.source.checked)
    this.checkbox[i].checked = e.target.checked;
    this.sendDate()
  }

  async sendDate(){
    let start = this.dateOfStart.toISOString().split('T')[0];
    let stop = this.dateOfEnd.toISOString().split('T')[0];

    // console.log('start',start)
    // console.log('end',stop)

    await this.delay(100);
    this.postData = {
      'start':await start,
      'stop': await stop,
    };

    this.datafreq.length = 0
    this.dataReactive.length = 0
    this.dataSelect.length = 0
    this.dataduration.length = 0
    
    let active = []
    await this.http.post((this.url+'/select/active'),this.postData).toPromise().then(data => {
      Object.keys(data).map(function(key){          
        for(var i in ({[key]:data[key]})){
          let device = []
          for(var j in ({[key]:data[key]})[i] ){
            device.push(({[key]:data[key]})[i][j])
          }
          active.push(device);
        }
    });
    })
    let reactive = []
    await this.http.post((this.url+'/select/reactive'),this.postData).toPromise().then(data => {
      Object.keys(data).map(function(key){          
        for(var i in ({[key]:data[key]})){
          let device = []
          for(var j in ({[key]:data[key]})[i] ){
            device.push(({[key]:data[key]})[i][j])
          }
          reactive.push(device);
        }
    });
    })
    let freq = []
    await this.http.post((this.url+'/hour/freq'),this.postData).toPromise().then(data => {
      Object.keys(data).map(function(key){          
        for(var i in ({[key]:data[key]})){
          let device = []
          for(var j in ({[key]:data[key]})[i] ){
            device.push(({[key]:data[key]})[i][j])
          }
          freq.push(device);
        }
    });
    })

    let duration = []
    await this.http.post((this.url+'/hour/duration'),this.postData).toPromise().then(data => {
      Object.keys(data).map(function(key){          
        for(var i in ({[key]:data[key]})){
          let device = []
          for(var j in ({[key]:data[key]})[i] ){
            device.push(({[key]:data[key]})[i][j])
          }
          duration.push(device);
        }
    });
    })

    this.dv = active;
    this.device_reactive = reactive;
    this.device_freq = freq
    this.device_duration = duration
    await this.delay(50);
    this.plot_active()
    this.plot_reactive()
    this.plot_frequency()
    this.plot_duration()
    
  }

  async plot_active(){
    let active = []
    let name = []
    let color =[]
    for (var i in this.checkbox){
      if (this.checkbox[i].checked == true){
        name.push(this.device_name[i])
        active.push(this.dv[i])
        color.push(this.colorlist[i])
      }
    }

    let ActivelineChart:any;
    ActivelineChart = new Chart('ActivelineChart', { 
      type: 'line', 
      data: { 
        labels:this.hours,
        datasets: [
          //label: 'Number of items sold in months',
          { data: active[0],
            label: name[0],
            // backgroundColor: color[0],
            fill: false,
            lineTension: 0.2,
            borderColor: color[0], 
            borderWidth: 2.5
          },
      ]
      },
      options: {responsive: true,
        maintainAspectRatio: false,
        // title: { 
        //   text: "Active Power",
        //   display: true,
        //   fontSize: 20
        // },
        scales: { 
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Time of Day',
              fontSize: 15
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Active Power(W)',
              fontSize: 15
            }
          }]
        }
      },
    })
    for (let i = 1; i < (Number(active.length));i++){
      ActivelineChart.data.datasets.push(
        { data: active[i],
          label: name[i],
          // backgroundColor: color[i],
          fill: false,
          lineTension: 0.2,
          borderColor: color[i], 
          borderWidth: 2.5
        })
    }
    
    ActivelineChart.update();
  }
  
  async plot_reactive(){
    let reactive = []
    let name = []
    let color = []
    for (var i in this.checkbox){
      if (this.checkbox[i].checked == true){
        name.push(this.device_name[i])
        reactive.push(this.device_reactive[i])
        color.push(this.colorlist[i])
      }
    }
    // console.log(this.device_name)
    // console.log(name,'test')
    let ReactivelineChart:any;
    ReactivelineChart = new Chart('ReactivelineChart', { 
      type: 'line', 
      data: { 
        labels:this.hours,
        datasets: [
          //label: 'Number of items sold in months',
          { data: reactive[0],
            label: name[0],
            // backgroundColor: color[0],
            fill: false,
            lineTension: 0.2,
            borderColor: color[0], 
            borderWidth: 2.5
          },
      ]
      },
      options: {responsive: true,
        maintainAspectRatio: false,
        // title: { 
        //   text: "Active Power",
        //   display: true,
        //   fontSize: 20
        // },
        scales: { 
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Time of Day',
              fontSize: 15
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Rective Power(W)',
              fontSize: 15
            }
          }]
        }
      },
    })
    for (let i = 1; i < (Number(reactive.length));i++){
      ReactivelineChart.data.datasets.push(
        { data: reactive[i],
          label: name[i],
          // backgroundColor: color[i],
          fill: false,
          lineTension: 0.2,
          borderColor: color[i], 
          borderWidth: 2.5
        })
    }
    
    ReactivelineChart.update();
  }
  async plot_frequency(){
    let freq = []
    let name = []
    let color = []
    for (var i in this.checkbox){
      if (this.checkbox[i].checked == true){
        name.push(this.device_name[i])
        freq.push(this.device_freq[i])
        color.push(this.colorlist[i])
      }
    }
      // frequency
      const freqchart = new Chart('freqchart', { 
        type: 'line', 
        data: { 
          labels:this.hours,
          datasets: [
           // label: 'Number of items sold in months',
            { data: freq[0],
              label: name[0],
              fill: false,
              lineTension: 0.2,
              // backgroundColor: color[0],
              borderColor: color[0], 
              borderWidth: 2.5
            }
        ]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'label'},
          maintainAspectRatio: false,
          // title: { 
          //   text: "Frequency (24-Hr)",
          //   display: true,
          //   fontSize: 20
          // },
          scales: { // แสดง scales ของแผนภูมิเริมที่ 0
            xAxes: [{
              stacked: true,
              display: true,
              ticks: {
                fontSize: 12
            },
              scaleLabel: {
                display: true,
                labelString: 'Hour of Day',
                fontSize: 15
              }
            }],
            yAxes: [{
              stacked: true,
              display: true,
              scaleLabel: {
                display: true,
                labelString: '%ON',
                fontSize: 15
              }
            }]
          }
        },
      })
      for (let i = 1; i < (Number(freq.length));i++){
        freqchart.data.datasets.push(
          { data: freq[i],
            label: name[i],
            // backgroundColor: color[i],
            fill: false,
            lineTension: 0.2,
            borderColor: color[i], 
            borderWidth: 2.5
          })
      }
      
      freqchart.update();

  }

  async plot_duration(){
    let d_key =['1M','5M','15M','30M','1H','2H','4H','>4H']
    let duration = []
    let name = []
    let color = []
    // console.log(this.device_duration)
    for (var i in this.checkbox){
      if (this.checkbox[i].checked == true){
        name.push(this.device_name[i])
        duration.push(this.device_duration[i])
        color.push(this.colorlist[i])
      }
    }

      // frequency
      const durationchart = new Chart('durationchart', { 
        type: 'line', 
        data: { 
          labels:d_key,
          datasets: [
           // label: 'Number of items sold in months',
            { data: duration[0],
              label: name[0],
              fill: false,
              lineTension: 0.2,
              // backgroundColor: color[0],
              borderColor: color[0], 
              borderWidth: 2.5
            }
        ]
        },
        options: {
          responsive: true,
          tooltips: {
            mode: 'label'},
          maintainAspectRatio: false,
          // title: { 
          //   text: "Frequency (24-Hr)",
          //   display: true,
          //   fontSize: 20
          // },
          scales: { 
            xAxes: [{
              stacked: true,
              display: true,
              ticks: {
                fontSize: 12
            },
              scaleLabel: {
                display: true,
                labelString: 'Hour of Day',
                fontSize: 15
              }
            }],
            yAxes: [{
              stacked: true,
              display: true,
              scaleLabel: {
                display: true,
                labelString: '%ON',
                fontSize: 15
              }
            }]
          }
        },
      })
      for (let i = 1; i < (Number(duration.length));i++){
        durationchart.data.datasets.push(
          { data: duration[i],
            label: name[i],
            // backgroundColor: color[i],
            fill: false,
            lineTension: 0.2,
            borderColor: color[i], 
            borderWidth: 2.5
          })
      }
      
      durationchart.update();

  }
  delay(delayInms) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

}
