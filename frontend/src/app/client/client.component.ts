import { Component, OnInit, AfterContentInit, ComponentFactoryResolver, ɵConsole } from '@angular/core';
import { Chart } from 'chart.js';
import { ClientService } from './client.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { DeviceService } from '../device.service'
import { BLACK_ON_WHITE_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
// import 'chart.piecelabel.js';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl} from '@angular/forms';
import { environment}  from '../../environments/environment'

@Component({
 selector: 'app-client',
 templateUrl: './client.component.html',
 styleUrls: ['./client.component.css'],
 providers: [ClientService]
})
//implements On Init
export class ClientComponent {

 today: number = Date.now();
 
 selectDate: FormGroup;
 constructor(
  private http: HttpClient,
  private clientService: ClientService,
  private service:DeviceService,
   

) 
{ 
  setInterval(() => {
  this.updateBackend()
  this.today = Date.now();
  this.getCheck()
  this.getRealtimeAppliance()
  this.Piechart()
  this.getSummary()
  }, 25000); 
}

  public lastcheckin:any
  public datasum:any[] = []; 
  public hours = ['12PM','1AM','2AM','3AM','4AM','5AM','6AM',
 '7AM','8AM','9AM','10AM','11AM','12PM','1PM','2PM','3PM',
 '4PM','5PM','6PM','7PM','8PM','9PM','10PM','11PM']


   public max
   public min
   public mean
 
   public realtime_appliance:[];

//new
   public array;
   public realtimeAppliance=[];
   public pie = [];
   public histChart: Chart;
   public d_y = [];
   public d_t = [];
   public label = [];
   public data = [];
   public colorlist = ['rgba(84, 46, 113,0.5)', 'rgba(251, 54, 64,0.5)', 'rgba(253, 202, 64,0.5)', 'rgba(167, 153, 183,0.7)',
  'rgba(255, 201, 150.0.7)', 'rgba(255, 132, 116, 0.5)','rgba(159, 95, 128,0.5)', 'rgba(88, 61, 114,0.5)',
  'rgba(25, 67, 80,0.5)', 'rgba(255, 136, 130,0.7)', '#ffc2b4', '#9dbeb9',
  'rgba(75, 119, 141,0.5)', 'rgba(40, 181, 181,0.7)', '#8fd9a8', '#d2e69c'
  ]
 
  public slide  =  0;
  public check =[];
  showstatus: boolean = true;
  showpie: boolean = true;
  public device_name = []
  public device_pie  = []
  public device_week = []
  public device_month = []
  public status  = []
  public summary = [];


  dateRange = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  public url = environment.flaskhost;
  public postData:any;

 
 async ngOnInit() {
  this.getCheck()
  console.log(this.url,'-> url')
  this.device_name = await this.clientService.getDevicename()
  await this.delay(500);
  this.device_name = this.device_name[0]
  this.device_pie = await this.clientService.getPie();
  this.datasum = await this.clientService.getSum();
  this.dateRange.value.start =new Date("2021-03-01");
  this.dateRange.value.end = new Date()
  this.sendDate()
  this.Piechart()
  this.getRealtimeAppliance()
  this.getSummary()

  
 }

 async sendDate (){
  let start = this.dateRange.value.start.toISOString().split('T')[0]
  let stop = this.dateRange.value.end.toISOString().split('T')[0]
  let week = []
  let month= []
  // console.log(start,stop)
  await this.delay(300);
   this.postData = {
     'start':await start,
     'stop': await stop,
   };
   await this.http.post((this.url+'/week'),this.postData).toPromise().then(data => {
    week.push(data)
   })
   
   await this.http.post((this.url+'/month'),this.postData).toPromise().then(data => {
    month.push(data)
  })
  
   this.device_week = week[0]
   this.device_month = month[0]
   this.consumtion()
}

async getCheck(){
  this.check  = await this.clientService.getCheck_decive();
  this.delay(500)
  

  // this.showpie = (Boolean(check[0]))  
  // this.showstatus = (Boolean(check[0])) 
 
}
 async getSummary(){
  this.datasum = await this.clientService.getSum();
  await this.delay(500);
  this.summary =  this.datasum.length > 0  ? this.datasum[0] : [];
  // console.log(this.summary);
  
  
}
async getRealtimeAppliance(){
  if (this.check[0][0] == 1){
    this.showpie = true
    this.showstatus = true
  }else{
    this.showpie = false
    this.showstatus = false
  }
  let realtime_appliance = await this.clientService.getRealtime_appliance()
  await this.delay(500);
  this.status = realtime_appliance[0]
  this.lastcheckin = 0

  // console.log(this.status.length,'status')
} 
async updateBackend(){
  // let results
  
  this.http.get(this.url+'/updatedata', {responseType: 'text'})
  .subscribe(data => data = data)
  // this.http.get(this.url+'/updatedata').subscribe(data => {
  // });
  console.log('sync success')    
}

async Piechart(){
  this.device_pie = await this.clientService.getPie();
  await this.delay(500);
  const pieChart = new Chart('pieChart', {
    type: 'doughnut',
    data: {
      labels: this.device_name,
      datasets: [
       // label: 'Number of items sold in months',
        { data: this.device_pie[0],
          label: 'Power Active',
          // fontColor: 'black',
          backgroundColor: this.colorlist,
          lineTension: 0.2,
          borderColor: "white",
          borderWidth: 1
        },
    ],
    },
    // showDatapoints: true,
    options: {
      // scaleShowLabels: false,
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Active power usage',
            fontSize:20
        },
        // pieceLabel: {
        //   render: 'value',
        //   fontSize: 50,
        //   // render: function (args) {
        //   //   const label = args.label,
        //   //         value = args.value;
        //   //   return label + ': ' + value;
        //   // },
        // },
}  
})
}

async consumtion(){
  let week= ['Mon','','Wed',' ','Fri',' ','Sun']
  let month= ['Jan','','Mar','','May','','Jul','','Sep','','Nov','']
  await this.delay(500)
  const weekChart = new Chart('weekChart', { 
    type: 'line', 
    data: { 
      labels:week,
      datasets: [
       // label: 'Number of items sold in months',
        { data: this.device_week,
          // label: 'Active Power',
          lineTension: 0.2,
          // backgroundColor: 'rgba(153, 255, 153, 0.2)',
          fill: false,
          borderColor: 'rgba(84, 46, 113,0.5)', 
          borderWidth: 2.5
        },
    ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
     },
      // title: { 
      //   text: meter,
      //   display: true
      // },
      scales: { // แสดง scales ของแผนภูมิเริมที่ 0
        xAxes: [{
          display: true,
          gridLines: {
            drawOnChartArea:false
        },
          ticks: {
            fontSize: 12
        },
          scaleLabel: {
            display: false,
            labelString: 'Time of Day',
            fontSize: 15
          }
        }],
        yAxes: [{
          display: true,
          gridLines: {
            drawOnChartArea:false
        },
          scaleLabel: {
            display: true,
            labelString: 'kWh',
            fontSize: 15
          }
        }]
      }
    },
  })
  const monthChart = new Chart('monthChart', { 
    type: 'line', 
    data: { 
      labels:month,
      datasets: [
       // label: 'Number of items sold in months',
        { data: this.device_month,
          // label: 'Active Power',
          lineTension: 0.2,
          fill: false,
          // backgroundColor: 'rgba(153, 255, 153, 0.2)',
          borderColor: 'rgba(84, 46, 113,0.5)', 
          borderWidth: 2.5
        },
    ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        display: false
     },
      // title: { 
      //   text: meter,
      //   display: true
      // },
      scales: { // แสดง scales ของแผนภูมิเริมที่ 0
        xAxes: [{
          display: true,
          gridLines: {
            drawOnChartArea:false
        },
          ticks: {
            fontSize: 12
        },
          scaleLabel: {
            display: false,
            labelString: 'Time of Day',
            fontSize: 15
          }
        }],
        yAxes: [{
          display: true,
          gridLines: {
            drawOnChartArea:false
        },
          scaleLabel: {
            display: true,
            labelString: 'kWh',
            fontSize: 15
          }
        }]
      }
    },

  })

}

  delay(delayInms) {
   return new Promise(resolve => {
     setTimeout(() => {
       resolve(2);
     }, delayInms);
   });
 }

}
 

