import { dataCollect,dataSelect } from '../interfaces/data.interface'
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { dataPercent,dataFreq,h_appliance,realtime } from '../interfaces/data.interface'
import { from } from 'rxjs';
import { environment}  from '../../environments/environment'
@Injectable()
export class ClientService {
    constructor(private http: HttpClient ) { }

    
    url = environment.flaskhost;

    async getPie(): Promise<dataCollect[]> {
        let dataList = [];
        const urlEndpoint = `${this.url}/pie`;
        await this.http.get(urlEndpoint).subscribe(
            res => dataList.push(res)
        )
        return dataList;
    }


    async getSum() {
        let dataList = [];
        const urlEndpoint = `${this.url}/summary`;
        await this.http.get(urlEndpoint).subscribe(
            res => dataList.push(res)
        )
        return dataList;
    }


    async getRealtime_appliance() {
        let dataList = [];
        const urlEndpoint = `${this.url}/realtime`;

        await this.http.get(urlEndpoint).subscribe(
            res => dataList.push(res)
        )
        return dataList;
    }

    
    async getCheck_decive (){
        let dataList=[];
        const urlEndpoint = `${this.url}/check`;
        await this.http.get(urlEndpoint).subscribe(
            res=> dataList.push(res)
        )
        return dataList;
    }
    async getDevicename (){
        let dataList=[];
        const urlEndpoint = `${this.url}/devicename`;
        await this.http.get(urlEndpoint).subscribe(
            res=> dataList.push(res)
        )
        return dataList;
    }
    // async Updatedata (){
    //     let dataList=[];
    //     const urlEndpoint = `${this.url}/updatedata`;
    //     await this.http.get(urlEndpoint).subscribe(
    //         res=> dataList.push(res)
    //     )
    //     return dataList;
    // }    


}