import { Component } from '@angular/core';
import { MatButtonModule} from '@angular/material/button';
import { MatDialog,MatDialogRef,MatDialogConfig} from '@angular/material/dialog';
import { CreateDeviceComponent } from '../create-device/create-device.component';
import { FormGroup,FormControl} from'@angular/forms';
import { DeviceService } from '../device.service'
import { MatTableDataSource } from '@angular/material/table';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { HttpClient } from '@angular/common/http';
import { environment}  from '../../environments/environment'

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent {
  constructor(public dialog:MatDialog,public service:DeviceService,
    private http:HttpClient
    //  public dialogRef:MatDialogRef<CreateDeviceComponent>
     ) { }
  public url = environment.flaskhost;
  public datal;
  public head = ['devicelName'];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['deviceName','action'];
  ngOnInit() {
    this.service.getDevice().subscribe(
      list => {
      let array = list.map(item =>{
        return {
          $key: item.key,
          ...item.payload.val()
        };
      });
      this.listData = new MatTableDataSource(array);
      console.log(array,'show array')
      this.datal = array;
      console.log('hi',this.datal,'type',typeof(this.datal))
    })

 }
  
  openDialog(){
    let dialogRef = this.dialog.open(CreateDeviceComponent);
  }

  OnEdit(row){
    // this.service.updateDevice(row)
    this.service.populateForm(row)
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(CreateDeviceComponent,dialogConfig)
  }

  onDelete($key){
    console.log($key)
    // if(confirm('Are you sure to delete this record ?')){
    this.service.deleteDevice($key);
    this.updateBackend()
    // this.notificationService.warn('! Deleted successfully');
    // }
    // }
  }
  updateBackend(){
    let results
    this.http.get('http://127.0.0.1:5000/updatedata').subscribe(data => {
      results = data;
    });
    console.log(results)    
  }



  
    


}
