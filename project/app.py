from flask import Flask , render_template,redirect, url_for,request
from flask_cors import CORS,cross_origin

from flask_jsonpify import jsonify
import json

import pandas as pd  
import numpy as np   
import statistics

import time
from datetime import datetime,timedelta,timezone
from dateutil.relativedelta import relativedelta

import pyrebase


app = Flask(__name__)
CORS(app)
cross_origin()

app.config['CORS_HEADERS'] = 'Content-Type'

config = {
    "apiKey": "AIzaSyCfUdRVttOgMgWpZsnbkRwxSu9Y-1NgbMU",
    "authDomain": "nilm-project-48e8c.firebaseapp.com",
    "databaseURL": "https://nilm-project-48e8c-default-rtdb.firebaseio.com",
    "projectId":  "nilm-project-48e8c",
    "storageBucket":"nilm-project-48e8c.appspot.com",
    "messagingSenderId": "376382069872",
    "appId": "1:376382069872:web:4e6889f79d73d9942ae8b5"
  }  
firebase = pyrebase.initialize_app(config)
db_firebase =firebase.database()

"""
All functions
"""
def ennergy_consumtion(df):
    # df = pd.read_csv('data/datafirebase.csv')
    data = df[df.Datetime!='0']
    data.Datetime = pd.to_datetime(data.Datetime)
    data = data.groupby('Datetime').sum()
    df = df[df.Datetime != '0']
    df['Datetime'] = pd.to_datetime(df['Datetime'])
    
    #mean hour 
    d = df.groupby('Datetime').sum()
    h =((pd.to_datetime(d.index).strftime('%Y-%m-%d %H')))
    h = pd.to_datetime(h)
    d = d.groupby(h).mean()
    
    #sumday
    day = ((pd.to_datetime(d.index).strftime('%Y-%m-%d')))
    d = d.groupby(day).sum()
    d.index = pd.to_datetime(d.index)
    df = pd.DataFrame()
    df['active'] = d.active
    df.to_csv('data/ensumtion.csv')
    return (df)

def groupdata_to_1_min (data):
    val = ['active','PF']
    df_list = []
    for i in val:
        t = pd.pivot_table(data, values = i, index=['Datetime'], columns = 'appliance')
        t = (t[t.index != 0].fillna(0))
        t = (t[t.index != '0'].fillna(0))
        t['Datetime'] = pd.to_datetime(t.index)
        df_list.append((t.groupby(pd.Grouper(key='Datetime', freq='1min')).mean()).fillna(0))
    return df_list

def oneday (dayall):
    # start = datetime.today().strftime("%Y-%m-%d")
    # stop = datetime.today().strftime("%Y-%m-%d %H:%M")
    start = (dayall.index[len(dayall)-1]).strftime('%Y-%m-%d')
    stop = (dayall.index[len(dayall)-1]).strftime("%Y-%m-%d %H:%M")
    rng = pd.date_range(start,stop, freq='1min')
#     dayall = dayall.drop(columns = ['timestamp'])
    oneday = dayall.reindex(rng,fill_value=0)
    return oneday

def data_firebase ():
    df = pd.read_csv('data/datafirebase.csv')
    df = df.drop(['Unnamed: 0'],axis=1)
    df = df.sort_values(by=['appliance'])
    return df

def cvhour (data,device_name):
    device_list = []
    for i in data.columns:
        if i != 'Datetime':
            appliance = pd.DataFrame(data[i])
            list_hour = [i for i in range(24)]
            appliance['Datetime'] = pd.to_datetime(data.Datetime)
            appliance['hour'] = appliance['Datetime'].dt.hour
            appliance_hour = appliance.groupby('hour').mean()
            appliance_hour = appliance_hour.reindex(list_hour)
            appliance_hour = appliance_hour.fillna(0)
            device_list.append(appliance_hour.round(2))
    return device_list
 
def en_hour (l,device_name):
    energy = pd.DataFrame(columns = device_name)
    for i in range(len(energy.columns)):
        energy[energy.columns[i]] = l[i][l[i].columns[0]]
    energy.index = (l[0].index)
    energy = energy.fillna(0)
    energy = energy.drop('Datetime',axis=1)
    energy = energy.to_json()
    return  energy

def duration(appliance):
    on_appliance = pd.DataFrame()
    appliance.index = pd.to_datetime(appliance['Datetime'])
    appliance.resample('T')
    appliance = appliance.sort_index()

    start = (appliance['Datetime'][0])
    end = appliance['Datetime'][len(appliance['Datetime'])-1]

    # all_times = pd.date_range(start, end, freq='T')
    # appliance= appliance.reindex(all_times)
    on_appliance['active'] = appliance[appliance.columns[0]] > 10
    onlist = []
    count_true = 0
    for i in (on_appliance['active']):
        if i == True:
            count_true += 1
        else:
            if count_true > 0:
                onlist.append(count_true)
                count_true = 0
    dfon = pd.DataFrame(onlist, columns=['on'])
    on_list=[]
    for i in dfon['on']:
        if i <= 1:
            on_list.append('1M')
        elif i <= 5:
            on_list.append('5M')
        elif i <= 15:
            on_list.append('15M')
        elif i <= 30:
            on_list.append('30M')
        elif i <= 60:
            on_list.append('1H')
        elif i <= 120:
            on_list.append('2H')
        elif i <= 240:
            on_list.append('4H')
        elif i > 240:
            on_list.append('>4H')
        else:
            on_list.append('1s')
    corindex =['1M','5M','15M','30M','1H','2H','4H','>4H']
    dfon['duration_on'] = on_list
    total_on = dfon['on'].sum()
    total_on 
    on_list_percent =[]
    for i in range(len(dfon['on'])):
        calpercent = ((dfon['on'][i]) * 100) / total_on
        on_list_percent.append(calpercent)
    dfon['percen_on'] = on_list_percent
    on_= dfon.groupby(['duration_on']).sum()
    on_= on_.reindex(corindex)
    on_ = on_.fillna(0)
    return on_.round(2)
def updatedata():
    #0 get device name
    read = db_firebase.child("device").get()
    device = read.each()
    device_list = [((l.val())) for l in device]
    device_df = pd.DataFrame(device_list)
    dv = []
    for i in range(len(device_df.deviceName)):
        dv.append('device'+str(i+1))
    device_list = pd.DataFrame(device_list)
    device_list.to_csv('data/devicename.csv')

    #1 get data
    read = db_firebase.child("data").get()
    if read.val() == 0:
        df = pd.DataFrame()
    else:
        g = pd.DataFrame(list((read.val()).values()))
        frame = []
        for i in range(len(g.columns)):
            frame.append(pd.DataFrame([x for x in g[i]]))
        df = pd.concat(frame)
        df = (df.sort_values('appliance')).reset_index(drop=True)
    device_df = pd.DataFrame()
    device_df['appliance'] =  dv

    db_firebase.child("data").set(0)

    #     #2 convert format data to device name and value
    if df.empty != True:
        db = df
        db['active'] = db['Active']
        db = db.drop(['Active'],axis=1)
        data = pd.merge(db, device_df, how="right", on=["appliance"]).fillna(0)


    # #group in 1 min and check date now
    # start = datetime.today().strftime("%Y-%m-%d")
    tz = timezone(timedelta(hours = 7))
    start = (datetime.now(tz=tz)).strftime("%Y-%m-%d")

    stop = str(datetime.now(tz=tz)+relativedelta(days=1))[:10]
    if df.empty != True:
        ond = groupdata_to_1_min(data)[0] 
        mask = (ond.index > start) & (ond.index < stop)
        ond = ond.loc[mask]


        #3 group one day
    oneday = pd.read_csv('data/oneday.csv')
    oneday = oneday.drop(['Unnamed: 0'],axis =1)

    # oneday['Datetime'] = (oneday[oneday.columns[0]])
    # oneday = oneday.drop(str(oneday.columns[0]),axis=1)

    if df.empty != True:
        ond['Datetime'] = ond.index
        ond = ond.reset_index(drop = True)
        check = oneday.Datetime[len(oneday)-1]
        print(check,start,'show check start')
        if(check[:10] ==  start):
            oneday = (pd.concat([oneday,ond])).reset_index(drop = True)
        else:
            ond = ond.reset_index(drop = True)
            oneday = ond
    else:
        oneday = oneday
        return ('success')
    oneday.to_csv('data/oneday.csv')

    #store data
    datafirebase = pd.read_csv('data/datafirebase.csv', error_bad_lines=False)
    datafirebase = datafirebase.drop(['Unnamed: 0'],axis =1)
    datafirebase = (pd.concat([datafirebase,df])).sort_values('appliance').reset_index(drop=True)
    datafirebase = datafirebase.drop(['Active'],axis = 1)
    datafirebase.to_csv('data/datafirebase.csv')

    #energy consumption
    energy = ennergy_consumtion(pd.read_csv('data/datafirebase.csv'))
    energy.to_csv('data/ensumtion.csv')

    #5 historical
    dayall = pd.read_csv('data/dayall.csv')
    dayall_reactive = pd.read_csv('data/dayall_reactive.csv')
    dayall = dayall.drop(['Unnamed: 0'],axis = 1)
    dayall_reactive = dayall_reactive.drop(['Unnamed: 0'],axis = 1)
    j = groupdata_to_1_min(df)
    reactive = np.sqrt(((j[0]**2)/j[1])-j[0]**2).fillna(0)
    active = j[0]
    active['Datetime'],reactive['Datetime'] = active.index,reactive.index
    active = (pd.concat([dayall,active]).fillna(0)).reset_index(drop = True)
    reactive = (pd.concat([dayall,reactive]).fillna(0)).reset_index(drop = True)
    active, reactive = active.groupby('Datetime').mean(),reactive.groupby('Datetime').mean()
    active['Datetime'],reactive['Datetime'] = (active.index),(reactive.index)
    active,reactive = (active.reset_index(drop =True)),(reactive.reset_index(drop =True))


    active.to_csv('data/dayall.csv')
    reactive.to_csv('data/dayall_reactive.csv')
    # db_firebase.child("data").set(0)

    return ('success')

def recheck():
    data = (pd.read_csv('data/oneday.csv')).fillna(0)
    data = data.sort_values('Datetime')
    tz = timezone(timedelta(hours = 7))
    start = datetime.now(tz=tz).strftime("%Y-%m-%d")
#     start = '2021-05-07'
    print (data.Datetime[len(data)-1][:10],'and',start)
    if data.Datetime[len(data)-1][:10] == start:
        check = True
    else:
        check = False
    return (check)

"""
path of Backend
"""
@app.route("/", methods=["GET"])
def helloWorld():
    updatedata()
    return jsonify('connect success')

@app.route('/updatedata')
def update():
    updatedata()
    return 'success'

def on_check(dataset):
    time = [i for i in range(24)]
    data = pd.DataFrame()
    dataset.resample('T')
    dataset = dataset.sort_index()
    start = (dataset.index[0])
    end = dataset.index[len(dataset.index)-1]
    all_times = pd.date_range(start, end, freq='T')
    dataset= dataset.reindex(all_times)
    data['active'] = dataset[dataset.columns[0]] > 10
    data['hour'] = dataset['hour']
    data1 = data.groupby('hour')['active'].apply(lambda x: (x==True).sum()).reset_index(name='count')
    data1 = data1.set_index('hour')
    data1['hour'] = data1.index
    data1 = data1.reindex(time)
    data1 = data1.fillna(0)
    return data1

def percent(dataset):
    data = on_check(dataset)
    total = data['count'].sum()
    data['percent'] = ""
    for i in range(len(data)):
        data['percent'][i] = ((data['count'][i]) * 100) / total
    data = data.drop(columns=['count','hour'])
    print('connect freq success')
    return data

@app.route("/pie", methods=["GET"])
def pie():
    data = pd.read_csv('data/oneday.csv')
    data =data.drop(['Unnamed: 0'],axis=1)
    l  =len(pd.read_csv('data/devicename.csv'))
    data = data[data.columns[:l+1]] 
    active_max = []
    if data.empty == False:
        d = data.groupby('Datetime').mean()
        h =((pd.to_datetime(d.index).strftime('%Y-%m-%d %H')))
        h = pd.to_datetime(h)
        d = d.groupby(h).mean()
        for i in d.columns:
            active_max.append(sum(d[i]))
    else:
        active_max = ([0 for i in range(len(data.columns))])
    return jsonify(active_max)

@app.route("/realtime", methods=["GET"])
def status ():
    data = pd.read_csv('data/oneday.csv')
    data =data.drop(['Unnamed: 0'],axis=1)
    l  =len(pd.read_csv('data/devicename.csv'))
    data = data[data.columns[:l+1]].fillna(0)
    # date = [data.index[-1]]
    if data.empty != True:
        
        status = []
        status = (data[data.index == data.index[-1]]['Datetime'].values.tolist())
        for i in data.columns:
            if i != 'Datetime':
                status.append(float(data[data.index == data.index[-1]][i]))
        
    else:
        status = ([0 for i in range(len(data.columns))])
        tz = timezone(timedelta(hours = 7))
        status[0] = str(datetime.now(tz=tz))
    status[0] = status[0][:16]
    return jsonify(status)

@app.route("/summary", methods=["GET"])
def summary ():
    if recheck() == True:
        data = pd.read_csv('data/oneday.csv')
        # data.index = data['Unnamed: 0']
        data =data.drop(['Unnamed: 0'],axis=1).fillna(0)
        l  =len(pd.read_csv('data/devicename.csv'))
        data = data[data.columns[:l+1]]
        active_max = []
        active_min = []
        active_avg = []
        d = data.groupby('Datetime').mean()
        h =((pd.to_datetime(d.index).strftime('%Y-%m-%d %H')))
        h = pd.to_datetime(h)
        d = d.groupby(h).mean()
        active = []
        for i in d.index:
            c = (d[d.index==i])
            active.append(sum((c.values)[0]))
        summary = [round(max(active),2),round(min(active),2),round(statistics.mean(active),2)]
    else:
        summary = ['-','-','-']
    return jsonify(summary)

@app.route("/devicename", methods=["GET"]) 
def devicename():
    name = pd.read_csv('data/devicename.csv')
    return jsonify(name[name.columns[-1]].tolist())
def devicename2():
    read = db_firebase.child("device").get()
    device = read.each()
    device_list = [((l.val())) for l in device]
    device_df = pd.DataFrame(device_list)
    # read = data_firebase()
    return  jsonify((device_df['deviceName'].unique()).tolist())  


@app.route("/select/active",methods=['POST'])
def postfreq_all ():
    start = str (request.json['start'])
    stop = str (request.json['stop'])
    datalist = []
    data = pd.read_csv('data/dayall.csv')
    data = (data.drop(['Unnamed: 0'],axis =1))
    d_name = data.columns[1:]
    stop = datetime.strptime(stop, '%Y-%m-%d')
    stop = (str(stop+(relativedelta(days=1))))[:10]

    if (start != data.Datetime[0][:10] or stop != data.Datetime[len(data)-1][:10]):
        print(start,stop)
        mask = (data['Datetime'] > start) & (data['Datetime'] < stop)
        data = data.loc[mask]
    datalist = cvhour(data,d_name[1:])
    data = en_hour (datalist,d_name)
    return data

@app.route("/select/reactive",methods=['POST'])
def reactive ():
    start = str (request.json['start'])
    stop = str (request.json['stop'])
    datalist = []
    data = pd.read_csv('data/dayall_reactive.csv')
    data = (data.drop(['Unnamed: 0'],axis =1))
    d_name = data.columns[1:]
    stop = datetime.strptime(stop, '%Y-%m-%d')
    stop = (str(stop+(relativedelta(days=1))))[:10]

    if (start != data.Datetime[0][:10] or stop != data.Datetime[len(data)-1][:10]):
        print(start,stop)
        mask = (data['Datetime'] > start) & (data['Datetime'] < stop)
        data = data.loc[mask]
    datalist = cvhour(data,d_name)
    data = en_hour (datalist,d_name)
    return data

@app.route("/hour/freq",methods=['POST'])
def hour_freq ():
    start = str (request.json['start'])
    stop = str (request.json['stop'])
    datalist = []
    freq_data = pd.read_csv('data/dayall.csv')
    # dayall = (dayall.drop(['Unnamed: 0.1'],axis =1))
    if (start != freq_data.Datetime[0][:10] or stop != freq_data.Datetime[len(data)-1][:10]):
        mask = (freq_data['Datetime'] > start) & (freq_data['Datetime'] < stop)
        freq_data = freq_data.loc[mask]
        freq_data.index = pd.to_datetime(freq_data.Datetime)
    for i in freq_data.columns[1:]:
        if i != 'Datetime':
            device = freq_data[i]
            device = pd.DataFrame(device)
            device['hour'] = device.index.hour
            datalist.append(percent(device).fillna(0))
    frequency = pd.DataFrame(columns = freq_data.columns[1:])
    for i in range(len(datalist)):
        frequency[frequency.columns[i]] = datalist[i]['percent']
        frequency.index = datalist[0].index
        frequency = frequency.round(2)
    return frequency.to_json()

@app.route("/hour/duration",methods=['POST'])
def hour_duration():
    start = str (request.json['start'])
    stop = str (request.json['stop'])

    start,stop = start[:10],stop[:10]
    datalist = []

    data = pd.read_csv('data/dayall.csv')
    # dayall = (dayall.drop(['Unnamed: 0.1'],axis =1))
    device_name = (data.columns[1:])

    stop = datetime.strptime(stop,'%Y-%m-%d')
    stop = (str(stop+(relativedelta(days=1))))[:10]

    if (start != data.Datetime[0][:10] or stop != data.Datetime[len(data)-1][:10]):
        print(start,stop)
        mask = (data['Datetime'] > start) & (data['Datetime'] < stop)
        data = data.loc[mask]
    for i in data.columns[2:]:
            appliance = pd.DataFrame(data[i])
            appliance['Datetime'] = pd.to_datetime(data.Datetime)
            appliance['hour'] = appliance['Datetime'].dt.hour
            if appliance.empty == True:
                h = pd.DataFrame(index = ['1M','5M','15M','30M','1H','2H','4H','>4H'])
                h['on'],h['percen_on']=  [0 for num in range(len(h))],[0 for num in range(len(h))]
            else:
                if appliance.columns[0] != 'Datetime':
                    h = duration(appliance)
            datalist.append(h)
    d = pd.DataFrame(columns = device_name)

    for i in range(len(datalist)):
        d[d.columns[i]] = datalist[i]['percen_on']
    d.index = (datalist[0].index)
    d = round(d.fillna(0),2)
    return d.to_json()



@app.route('/month', methods=['POST'])
def month():
    start = str(request.json['start'])
    stop = str(request.json['stop'])

    index = [1,2,3,4,5,6,7,8,9,10,11,12]
    d = pd.read_csv('data/ensumtion.csv')
    d.index = d['Unnamed: 0']
    d = d.drop(columns=['Unnamed: 0'])
    d = pd.read_csv('data/ensumtion.csv')
    d.index = d['Unnamed: 0']
    d = d.drop(columns=['Unnamed: 0'])
    
    stop = datetime.strptime(stop,'%Y-%m-%d')
    stop = (str(stop+(relativedelta(days=1))))[:10]
    print(start,stop)
        
    if (start != d.index[0][:10] or stop != d.index[len(d)-1][:10]):
        print(start,stop)
        mask = (d.index > start) & (d.index < stop)
        d = d.loc[mask]

    d.index = pd.to_datetime(d.index)
    d = d.groupby(d.index.month).mean()
    d = d.active / 1000
    d = d.reindex(index).fillna(0)
    d = d.round(2)
    return jsonify(d.values.tolist())


@app.route('/week', methods=['POST'])
def week():
    d = pd.read_csv('data/ensumtion.csv')
    d.index = d['Unnamed: 0']
    # d = d.drop(columns=['Unnamed: 0'])
    start = str(request.json['start'])
    stop = str(request.json['stop'])

    # start ='2021-03-01 '
    # stop = '2021-05-12'

    stop = datetime.strptime(stop,'%Y-%m-%d')
    stop = (str(stop+(relativedelta(days=1))))[:10]
    print(start,stop)

    index = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday']

    if (start != d.index[0][:10] or stop != d.index[len(d)-1][:10]):
        print(start,stop)
        mask = (d.index > start) & (d.index < stop)
        d = d.loc[mask]

    d.index = pd.to_datetime(d.index)
    d = d.groupby(d.index.weekday_name).mean()
    d = d.active / 1000
    d = d.reindex(index).fillna(0)
    d = d.round(2)
    return jsonify(d.values.tolist())

@app.route("/check",methods=['GET'])
def check_date():
    check = recheck()
    print(check)
    if  check == True:
        check = 1
    else:
        check = 0
    return jsonify([check])